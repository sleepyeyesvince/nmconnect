# nmconnect

A simple Linux script/CLI app to connect to an alternate or new WiFi access point.

Released under GPLv3: https://www.gnu.org/licenses/gpl-3.0.en.html

Dependencies: nmcli, xsel

## What this app does:
1. Checks if the dependencies required (nmcli, xsel) are installed.
2. If they are not installed, it will try to do so using commonly found package managers (apt, pacman, dnf or zypper).
3. If it is unable to install the required dependencies automatically, you will need to do so manually via your distribution's installation method.
4. Shows the current WiFi access point in use
5. Scan for available WiFi access points
6. Select and connect to the access point you would like to connect to. If it is a new AP, it will prompt you for the password

## How to use (if you don't have git):
1. Download or copy the script to a text file named "nmconnect"
2. Make the file executable by:
	* right clicking the file in your file manager, choosing properties and marking it as executable; OR
	* in terminal:
> `chmod +x nmconnect`
3. Execute the script by opening a terminal in the same folder and running:
> `./nmconnect`

## How to use (with git):
1. git clone this repository
> `git clone https://gitlab.com/sleepyeyesvince/nmconnect.git`
2. Make the file executable by:
	* right clicking the file in your file manager, choosing properties and marking it as executable; OR
	* in terminal:
> `cd nmconnect`
> `chmod +x nmconnect`
3. Execute the script by opening a terminal in the same folder and running:
> `./nmconnect`

[Optional: You could place the script in a folder contained in $PATH such has ~/bin or ~/.bin to be able to run the script in terminal from anywhere in your system]
